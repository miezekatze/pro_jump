#!/bin/bash

set -xe

CXX=g++
CXXFLAGS="-Wall -Werror -Wextra -O3 -pedantic" 

$CXX $CXXFLAGS main.cpp -o main

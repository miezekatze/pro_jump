#include <cctype>
#include <iostream>
#include <fstream>
#include <sstream>

#include <map>
#include <algorithm>
#include <vector>
#include <unistd.h>
#include <dirent.h>

void parse_special_args(std::string &r, std::vector<std::string> &args) {
    std::string tmp = "";
    for(char c : r) {
        switch(c) {
            case ')':
            case ',':
                args.push_back(tmp);
                tmp.clear();
                break;
            default:
                tmp.push_back(c);

        }
    }
}

void parse_special(std::stringstream &md, std::string &entry, int x, int y) {
    entry.erase(std::remove_if(entry.begin(), entry.end(), isspace), entry.end());

    std::string type, r;
    size_t pos = entry.find('(');

    if(pos == std::string::npos) {
        type = entry;
    } else {
        type = entry.substr(0, pos);
        r = entry.substr(pos + 1);
    }
    /*md << type;*/
    md.write((const char*)&x, sizeof(int));
    md.write((const char*)&y, sizeof(int));
    md.put('\0');
    md.put(type[0]);
    std::vector<std::string> args;
    parse_special_args(r, args);
    for(auto a : args) {
        md << a;
        md.put('\0');
    }
    md.put('\0');
}

void parse_list(std::stringstream &md, std::string &entry, int x, int y) {
    entry.erase(std::remove_if(entry.begin(), entry.end(), isspace), entry.end());

    std::string tmp;
    for(size_t i = 0; i < entry.size(); i++) {
        char c = entry[i];
        switch(c) {
            case ']':
            case ',': 
                if(tmp.empty()) break;
                md.write((const char*)&x, sizeof(int));
                md.write((const char*)&y, sizeof(int));
                md.put(tmp[0]);
                tmp.clear();
                break;
            case '{':
                for(size_t p = i + 1; p < entry.size(); p++) {
                    i = p;
                    if(entry[p] == '}') break;
                    tmp.push_back(entry[p]);
                }
                parse_special(md, tmp, x, y);
                tmp.clear();
                break;
            default:
                tmp.push_back(c);
        }
    }
}

void parse_map(std::stringstream &md, int &w, int &h, std::map<std::string, std::string> &defs, std::string &map) {
	bool sd = 0;
    char c = 0;
    std::string tmp;
	for(size_t i = 0; i < map.size(); i++) {
        c = map[i];
		if(isspace(c)) continue;
		switch(c) {
			case ';':
				h++;
				w = -1;
				break;
			case '.':
				break;
            case '{':
                tmp.clear();
                for(size_t p = i + 1; p < map.size(); p++) {
                    i = p;
                    if(map[p] == '}') break;
                    tmp.push_back(map[p]);
                }
                i++;
                parse_special(md, tmp, w, h);
                break;
            case '[':
                tmp.clear();
                for(size_t p = i + 1; p < map.size(); p++) {
                    i = p;
                    tmp.push_back(map[p]);
                    if(map[p] == ']') break;
                }
                parse_list(md, tmp, w, h);
                break;
			case 'P':
				if(sd) {
					std::cerr<<"The map may only contain one spawnpoint!"<<std::endl;
					exit(1);
				}
				sd = 1;
				// FALLTHROUGH
			default:
                tmp.clear();
                tmp.push_back(c);
                if(defs[tmp] != "") {
                    map = map.replace(i, 1, defs[tmp]);
                    i--;
                    continue;
                }
				md.write((const char*)&w, sizeof(int));
				md.write((const char*)&h, sizeof(int));
				md.put(c);
		}
		w++;
	}
	if(h == 0) {
		std::cerr<<"The map cannot have a height of 0!"<<std::endl;
		exit(1);
	}
	w = (map.size() - h) / h;
}

void parse_keep_effect(std::stringstream &md, std::string &def) {
    char position[] = {0, 0, 0, 0, 0, 0, 0, 0};
    md.write(position, 8);
    md.put('\xff');
    bool b = def == "True";
    md.put(*(char*)&b);
}

void parse_def(std::string &def, std::map<std::string, std::string> &defs) {
    std::string base = "", val = "";
    bool d = 0, pv = 0;
    for(char c : def) {
        if(isspace(c)) continue;
        switch(c) {
            case '-':
                d = 1;
                if(!pv) break;
                // FALLTHROUGH
            case '>':
                if(d && !pv) {
                    pv = 1;
                    break;
                } 
                // FALLTHROUGH
            default:
                if(pv) val.push_back(c);
                else base.push_back(c);
                d = 0;
        }
    }
    defs[base] = val;
}

void parse_hook(std::stringstream &md, std::map<std::string, std::string> &defs, std::string &entry) {
    std::string tmp = "";
    bool d = 0, pv = 0; 
    int x = 0, y = 0;
    for(char c : entry) {
        if(isspace(c)) continue;
        switch(c) {
            case '-':
                d = 1;
                if(!pv) break;
                // FALLTHROUGH
            case ':':
                d = 0;
                if(!pv) {
                    x = std::stoi(tmp);
                    tmp.clear();
                    break;
                }
                // FALLTHROUGH
            case '>':
                if(d && !pv) {
                    pv = 1;
                    y = std::stoi(tmp);
                    tmp.clear();
                    break;
                } 
                // FALLTHROUGH
            default:
                tmp.push_back(c);
                d = 0;
        }
    }
    if(defs.count(tmp)) tmp = defs[tmp];
    if(tmp[0] == '{')  {
        std::string se = "";
        for(size_t p = 1; p < tmp.size(); p++) {
            if(tmp[p] == '}') break;
            se.push_back(tmp[p]);
        }
        parse_special(md, se, x, y);
    } else {
        md.write((const char*)&x, sizeof(int));
        md.write((const char*)&y, sizeof(int));
        md.put(tmp[0]);
    }
}

int main(int argc, char**argv) {
	if(argc != 3) {
		std::cerr<<"Usage: "<<argv[0]<<" <infile> <outfile>"<<std::endl;
		return 1;
	}

	std::string input;

	std::ifstream fpi(argv[1], std::ios::in | std::ios::binary);
	if(!fpi) {
		std::cerr<<"Unable to open "<<argv[1]<<std::endl;
		exit(1);
	}
	input = {std::istreambuf_iterator<char>{fpi}, std::istreambuf_iterator<char>{}};
	fpi.close();

	bool bs = 0, qm = 0, cm = 0;
	std::string buffer, key;

	std::stringstream md;
	int w = 0, h = 0;

    std::string name;

    std::map<std::string, std::string> defs;

	for(char c : input) {
		if(c == '%') cm ^= 1;
		if(cm) continue;
		if(isspace(c) && !qm && !bs) continue;
		switch(c) {
			case '"':
				if(bs) {
					buffer.push_back(c);
					bs = 0;
				}
				if(qm) {
                    if(key == "MAP") parse_map(md, w, h, defs, buffer);
                    else if(key == "NAME") name = buffer;
                    else if(key == "DEF") parse_def(buffer, defs);
                    else if(key == "HOOK") parse_hook(md, defs, buffer);
                    else if(key == "KEEP_EFFECTS") parse_keep_effect(md, buffer);
					buffer.clear();
					qm = 0;
					break;
				}
				key = buffer;
				buffer.clear();
				qm = 1;
				break;
			case '\\':
				if(bs) buffer.push_back(c);
				bs ^= 1;
				break;
			case '\n':
				break;
			default:
				buffer.push_back(c);
				bs = 0;

		}
	}

	std::ofstream fp(argv[2], std::ios::out | std::ios::binary);
	if(!fp) {
		std::cerr<<"Unable to open "<<argv[2]<<std::endl;
		exit(1);
	}
	fp<<name;
	fp.put('\0');
	fp.write((const char*)&w, sizeof(int));
	fp.write((const char*)&h, sizeof(int));
	fp<<md.rdbuf();
    int mnend = -1;
	fp.write((const char*)&mnend, sizeof(int));
	fp.close();
}

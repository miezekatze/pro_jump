import java.util.List;
import java.util.Optional;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class MapData {
	public final String title;
	public final int w;
	public final int h;
	public final int sx, sy;
	public final List<MapEntry> entries;
    public final boolean keep_effects;
    public final String filename;

	public MapData(String title, int width, int height, List<MapEntry> entries, int sx, int sy, boolean keep_effects, String filename) {
		this.title = title;
		this.w = width;
		this.h = height;
		this.entries = entries;
		this.sx = sx;
		this.sy = sy;
        this.keep_effects = keep_effects;
        this.filename = filename;
	}
}

class SpecialEntry {
    public final char type;
    public final List<String> args;
    
    public SpecialEntry(char type, List<String> args) {
        this.type = type;
        this.args = args;
    }
}

class MapEntry {
	public final int x;
	public final int y;
	public final char c;

    public Optional<SpecialEntry> special = Optional.empty();

	public MapEntry(int x, int y, char c, Optional<SpecialEntry> special) {
		this.x = x;
		this.y = y;
		this.c = c;
        this.special = special;
	}
}

Optional<MapData> readMapData(String filename) {
	byte b[] = loadBytes(filename);
	if(b == null) return Optional.empty();

	StringBuilder name = new StringBuilder();
	for(byte i : b) {
		if(i == 0) break;
		name.append((char)i);
	}

	ByteBuffer bb = ByteBuffer.wrap(b, name.length() + 1, b.length - name.length() - 1).order(ByteOrder.LITTLE_ENDIAN);
	int w = bb.getInt();
	int h = bb.getInt();
	int sx = 0, sy = 0;
    boolean keep_effects = false;

	final List<MapEntry> entries = new ArrayList<MapEntry>();
	for(;;) {
		int x = bb.getInt();
        if(x == -1) {
            MapData data = new MapData(name.toString(), w, h, entries, sx, sy, keep_effects, filename);
            return Optional.of(data);
        }
		int y = bb.getInt();
		char c = (char)bb.get();
        Optional<SpecialEntry> special = Optional.empty();
        switch (c) {
        case '\0':
            char type = (char)bb.get();
            List<String> args = new ArrayList<String>();
            String tmp = "";
            for(;;) {
                char a = (char)bb.get();
                if(a == '\0') {
                    if(tmp.length() == 0) break;
                    args.add(tmp);
                    tmp = "";
                    continue;
                }
                tmp += a;
            } 
            SpecialEntry entry = new SpecialEntry(type, args);
            special = Optional.of(entry);
            break;
        case (char)(-1):
            keep_effects = bb.get() != 0;
            continue;
        case BlockType.PLAYER_SPAWN:
			sx = x;
			sy = y;
			continue;
        }
        entries.add(new MapEntry(x, y, c, special));
	}
}

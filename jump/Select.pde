import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

class LevelSelector {
    private List<MapData> data;
    private int current;
    private boolean select;

    public LevelSelector() {
        current = 0;
        select = true;
        data = new ArrayList<>();    
        
        Stream.of(new File(sketchPath() + "/assets/levels").listFiles()).filter(file -> !file.isDirectory()).map(File::getName).collect(Collectors.toSet()).forEach(level -> {
            readMapData(sketchPath() + "/assets/levels/" + level).ifPresentOrElse(entry -> {
                data.add(entry);
                println("Sucessfully loaded " + level + " ...");
            }, () -> {
                println("Error parsing " + level + " ...");
            });
        });
    }

    private float r, g, b;
    private color scale_color(int c) {
        r = red(c)   / 3;
        g = green(c) / 3;
        b = blue(c)  / 3;
        return color(r, g, b);
    }

    private int c;
    public void render() {
        c = height / data.get(current).h;
        data.get(current).entries.forEach(entry -> {
            fill(scale_color(getBlockColor(entry.c)));
            if (entry.c == BlockType.GHOST) return;
            if(entry.c == '\0') return;
            if(entry.x * c > width || entry.x * c + c < -width) return;
            rect((entry.x) * c, entry.y * c, c, c);
        });
        fill(#dd7777);
        textAlign(CENTER);
        textSize(100);
        text(data.get(current).title, width/2, 100);
    }

    public boolean selecting() {
        return select;
    }

    public void select_level() {
        select = true;
    }

    public void increase_selection() {
        current++;
        if(current == data.size()) current = 0;
    }

    public void decrease_selection() {
        current--;
        if(current < 0) current = data.size() - 1;
    }

    public MapData run() {
        select = false;
        return data.get(current);
    }
}

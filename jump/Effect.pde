import java.util.List;

class Effect {
    public int duration;
    public final float amp;
    public final char type;

    public Effect(char type, int duration, float amp) {
        this.duration = duration;
        this.amp = amp;
        this.type = type;
    }
}

class EffectHandler {
    public final List<Effect> active;
    private final List<Effect> obsolete;

    public EffectHandler() {
        active = new ArrayList<>();    
        obsolete = new ArrayList<>();    
    }

    private int it;
    public void tick(MapData level, MovementData ps) {
        textAlign(LEFT, TOP);
        active.removeAll(obsolete);
        obsolete.clear();
        it = 0;
        active.forEach(effect -> {
            if(effect.duration <= millis()) {
                switch(effect.type) {
                    case EffectType.POIS:
                        die(effect.amp < 1);
                        break;
                }
                obsolete.add(effect);
                return;
            }
            textSize(32);
            fill(getEffectColor(effect.type));
            text(getEffectName(effect.type) + " (" + effect.amp + "): " + (int)((effect.duration - millis()) / 1000), 2, 32 * it);
            it++;
            switch(effect.type) {
                case EffectType.SPEED:
                    ps.set_speed_mod(effect.amp);
                    break;
                case EffectType.ANTIDOTE:
                    obsolete.addAll(active.stream().filter(e -> (e.type == EffectType.POIS)).toList());
                    break;
            }
        });
    }

    public void create(char type, int duration, float amp) {
        obsolete.addAll(active.stream().filter(effect -> (effect.type == type)).toList());
        active.add(new Effect(type, millis() + duration, amp));
    }
    
    public void reset() {
        obsolete.addAll(active);
    }
}

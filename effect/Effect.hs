module Block where

import Prelude hiding (tail, head)
import Data.Char
import Data.Bifunctor

data Entry = Entry {
    name :: String,
    char :: Char,
    description :: String,
    color :: String
}

-- Parsing

tail :: [a] -> [a]
tail (x:xs) = xs
tail _ = []

head :: String -> Char
head (x:xs) = x
head [] = '\x00'

parseChar :: String -> Char
parseChar = head . takeWhile (/= '\'') . tail . dropWhile (/= '\'')

parseString :: String -> String
parseString = takeWhile (/= '"') . tail . dropWhile (/= '"')

getPart :: Int -> String -> String
getPart i s = if i == 0 then dropWhile isSpace (fst x) else getPart (i - 1) (snd x) where x = span (/= ',') $ dropWhile isSpace $ dropWhile (== ',') $ dropWhile isSpace s

parseEntry :: String -> Entry
parseEntry s = Entry {
    name        = parseString $ getPart 0 s,
    char        = parseChar $ getPart 1 s,
    description = parseString $ getPart 2 s,
    color       = parseString $ getPart 3 s
} 

fetchEntry :: String -> (String, String)
fetchEntry = second tail . span (/= '}') . tail . dropWhile (/= '{')

parseAll :: String -> [Entry]
parseAll [] = []
parseAll s = if fst x /= "" then (parseEntry . fst) x : parseAll (snd  x) else [] where x = fetchEntry s

-- Code Generation

genClassEntry :: [Entry] -> String
genClassEntry (e:es) = "    public static final char " ++ name e ++ " = " ++ show (char e) ++ ";\n" ++ genClassEntry es
genClassEntry [] = []

genClass :: [Entry] -> String
genClass e = "static class EffectType {\n" ++ genClassEntry e ++ "}\n"

genColorEntry :: [Entry] -> String
genColorEntry (e:es) = "        case EffectType." ++ name e ++ ":\n" ++ "            return " ++ color e ++ ";\n" ++ genColorEntry es
genColorEntry [] = []

genColor :: [Entry] -> String
genColor e = "int getEffectColor(char type) {\n    switch(type) {\n" ++ genColorEntry e ++ "        default:\n            return #00ff00;\n    }\n}\n"

genNameEntry :: [Entry] -> String
genNameEntry (e:es) = "        case EffectType." ++ name e ++ ":\n" ++ "            return " ++ show (description e) ++ ";\n" ++ genNameEntry es
genNameEntry [] = []

genName :: [Entry] -> String
genName e = "String getEffectName(char type) {\n    switch(type) {\n" ++ genNameEntry e ++ "        default:\n            return \"<Unknown Effect>\";\n    }\n}\n"

genAll :: [Entry] -> String
genAll e = "// Automatically generated file, do not edit!\n\n" ++ genClass e ++ "\n" ++ genColor e ++ "\n" ++ genName e

main :: IO ()
main = (parseAll <$> readFile "effects.fl") >>= writeFile "EffectType.pde" . genAll

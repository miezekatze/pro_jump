#!/bin/bash

set -xe

mklev() {
	pushd mklev
	sh build.sh
	popd
}

dat() {
	pushd levels
    mkdir --parents ../jump/assets/levels
	for F in $(find . -iname "*.des"); do
		../mklev/main $F ../jump/assets/levels/$(basename $F .des).lvl
	done
	popd
}

run() {
	pushd jump
	processing-java --sketch=../jump --run 
	popd
}

clean() {
	rm -f mklev/main
	rm -rf jump/assets/levels
    rm -f jump/Block.pde
    rm -f jump/Textures.pde
    rm -f jump/EffectType.pde
    rm -f block/Block.pde
    rm -f block/Textures.pde
    rm -f effect/EffectType.pde
}

mkblock() {
    pushd block
    sh build.sh
    cp Block.pde ../jump
    cp Textures.pde ../jump
    popd
}

mkeffect() {
    pushd effect
    sh build.sh
    cp EffectType.pde ../jump
    popd
}

case "$1" in
    all)
        mklev
        mkblock
        mkeffect
		dat
		run
        ;;
	dat)
		mklev
        mkblock
        mkeffect
		dat
		;;
    lvl)
        dat
        ;;
	run)
		run
		;;
	clean)
		clean
		;;
    *)
        $0 all
        ;;
esac
